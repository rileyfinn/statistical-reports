---
title: 'Data Analysis Report'
author: 'Riley Finn'
date: 'December 10th, 2018'
output: 'pdf_document'
fig.caption: yes
bibliography: bib.bib
---

```{r, echo=FALSE, include=FALSE}
library(knitr)
library(kableExtra)
library(cowplot)
library(car)
library(readxl)
library(dplyr)
library(ggplot2)
library(ggthemes)
library(gridExtra)
library(latex2exp)

islandData <- read_excel('data-collection.xlsx')

islandData <-
    islandData %>%
    rename('PreTest' = `PreTest (µg/dL)`,
           'PostTest' = `PostTest (µg/dL)`) %>%
    mutate(Difference = PostTest - PreTest)

controlData <- filter(islandData, Group == 'Control')
trtData <- filter(islandData, Group == 'Treatment')

### demographic table
island.demo <-
    islandData %>%
    group_by(Group) %>%
      summarize(
          SampleSize = n(),
          MeanAge = round(mean(Age), 1),
          SdAge = round(sd(Age), 1),
          MeanCortilsolLevel = round(mean(PreTest), 1),
          SdCortisolLevel = round(sd(PreTest), 1))
```
A variety of factors contribute to how people respond under stress.  Caffeine consumption is one factor that can influence the human body’s behaviors under stress.
Cortisol is a hormone that can influence human’s response to stress. When people are feeling stressed, cortisol level rises and releases glucose into the bloodstream [@do2015technical]. Cortisol can also help humans reduce inflammation and regulate metabolism [@brunner2006effect]. 

Cortisol’s influence on stress and its relationship with caffeine was carefully examined in this study. The study analyzed: Do adult women (18 years old - 80 years old) who drink espresso have increased cortisol levels compared to adult women (18 years old - 80 years old) who do not drink espresso?

```{r echo = FALSE, fig.align='center', fig.cap='Table 1'}
island.demo <-
    islandData %>%
    group_by(Group) %>%
      summarize(
          'Sample Size' = n(),
          'Mean Age' = round(mean(Age), 1),
          'Standard Deviation Age' = round(sd(Age), 1),
          'Mean Cortisol Level' = round(mean(PreTest), 1),
          'Std. Dev. Cortisol Level' = round(sd(PreTest), 1))

kable(island.demo, 'latex', booktabs = TRUE) %>%
    row_spec(0, bold = TRUE) %>%
    kable_styling(position = 'center',
                  font_size = 8)
```

The table shows there is little difference between the control and the treatment group. There is no reason to believe the small differences that are present will skew the results. If the means and standard deviations of the two groups were drastically different, there may have been reason to question the accuracy of the results.

```{r out.width = "60%", echo=FALSE, fig.align='center'}
yellow.ptol <- rgb(221, 170, 51, maxColorValue = 255)
blue.ptol <- rgb(0, 68, 136, maxColorValue = 255)
islandData %>%
    ggplot(aes(Group, Difference, fill = Group)) +
    geom_boxplot() +
    theme_tufte() +
    geom_hline(yintercept = c(0, 0.5, 1), alpha = 0.05) +
    guides(fill = FALSE) +
    theme(axis.title.y = element_text(size = 14),
          axis.title.x = element_blank(),
          axis.text = element_text(size = 14),
        plot.caption = element_text(size = 12, hjust = 0.5)) +
    labs(#caption = 'Figure 1',
         y = TeX("Cortisol Level Differences($\\mu$g/dL)")) +
    scale_fill_manual(values = c(yellow.ptol, blue.ptol)) + 
    labs(caption = 'Figure 1')
```

Examining the boxplot(figure 1), the control group's range of cortisol level differences does not deviate that far from zero. This small deviation implies that the cortisol levels for the control group did not change that much. The difference of the treatment group is centered around a value of one which suggests the treatment group's cortisol levels usually increased by one $\mu$ g/dL. 

A two sample t-test was used to analyze the difference between the cortisol levels of the control and treatment groups. In order to perform a two sample t-test, the cortisol levels had to meet certain requirements. One of these requirements is that each measurement must be independent of all other measurements. The other requirement is that the frequency of the measured values from each group must be normally distributed, that is, the frequency values must form a "bell-shape" (see figures 2 and 3 on page 2). In other words, the majority of each group's cortisol-level differences should be centered around the group's average cortisol-level difference.

```{r echo = FALSE, fig.width=8, fig.height=2, dpi=300}
ctrlDensPlot <-
    islandData %>%
    filter(Group == 'Control') %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() +
    xlim(-0.5, 0.5) +
    scale_fill_manual(values = yellow.ptol) +
    theme_tufte() +
    labs(y = 'Frequency',
         caption ='Figure 2') +
    guides(fill = FALSE) +
    theme(plot.caption = element_text(hjust = 0.5))

trtDensPlot <-
    islandData %>%
    filter(Group == 'Treatment') %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() +
    xlim(0.25, 1.5) +
    scale_fill_manual(values = blue.ptol) +
    theme_tufte() +
    labs(y = 'Frequency',
         caption = 'Figure 3') +
    guides(fill = FALSE) +
    theme(plot.caption = element_text(hjust = 0.5))

toGetLengend <-
    islandData %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() + 
    scale_fill_manual(values = c(yellow.ptol, blue.ptol))
legend <- get_legend(toGetLengend)

plots <- align_plots(ctrlDensPlot, legend, trtDensPlot, align = 'v', axis = 'l')
plot_grid(plots[[1]], plots[[2]], plots[[3]], nrow = 1)
```

Now that the initial requirements for the two sample t-test are met, the null and alternative hypotheses can be tested. The null hypothesis is as follows: there is no difference between the average difference of the control group's cortisol levels and the treatment group's cortisol levels. The alternative hypothesis is as follows: the average cortisol level difference for the treatment group is greater than the average cortisol level difference for the control group. The two-sample t-test yields a p-value. If the p-value is less than 0.05, then the null hypothesis is rejected. If the p-value is greater than 0.05, then the null hypothesis is not rejected nor accepted. 

Using R, the two sample t-test was performed and returned a p-value of approximately zero. A p-value of zero resulted in the null hypothesis being rejected. With overwhelming confidence, it can be concluded that the consumption of 60 mL of espresso increases the cortisol levels in females ages, 18 to 80 years old.

The demographic data of age, village, and house number were analyzed to dectect possible cofounders. Models were built (see pages 6-13 of Appendix) to determine if age or location influenced the cortisol levels. Whether the ages, villages, and house numbers were considered separately or combined, no evidence suggested the possibility of cofounders. It may safely be concluded that the differences in age, village, and house number influenced the cortisol levels in similar manners. For example, the 61 year-old Bridit Blomgren who lives in house 134 in Bloundous is affected by 60 mL of espresso in the same way as 26 year-old Laura Solberg who lives in house 845 in Vardo.

The relationship between caffeine and cortisol was carefully examined and a convincing conclusion was drawn. Hence, the reasearch objective was successfully accomplished. It is concluded that caffeine causes a significant increase in cortisol levels in females, ages 18 to 80.

\pagebreak

## Appendix

Packages
```{r}
library(knitr)
library(kableExtra)
library(cowplot)
library(car)
library(readxl)
library(dplyr)
library(ggplot2)
library(ggthemes)
library(gridExtra)
```

Data collected(and used for analyses)
```{r}
kable(islandData) %>%
  kable_styling(font_size = 7)
```

Checking assumptions and performing t-test
```{r}
controlData <- filter(islandData, Group == 'Control')
trtData <- filter(islandData, Group == 'Treatment')

par(mfrow = c(1, 2))
qqnorm(controlData$Difference)
qqline(controlData$Difference)

qqnorm(trtData$Difference)
qqline(trtData$Difference)

t.test(trtData$Difference, controlData$Difference, alternative = 'greater')
```

Demographic table
```{r, include=FALSE}
island.demo <-
    islandData %>%
    group_by(Group) %>%
      summarize(
          SampleSize = n(),
          MeanAge = round(mean(Age), 1),
          SdAge = round(sd(Age), 1),
          MeanCortilsolLevel = round(mean(PreTest), 1),
          SdCortisolLevel = round(sd(PreTest), 1))

kable(island.demo, 'latex', booktabs = TRUE) %>%
    row_spec(0, bold = TRUE) %>%
    kable_styling(position = 'center')
```

Figure 1
```{r, results = 'hide', echo=TRUE, fig.show = 'hide'}
yellow.ptol <- rgb(221, 170, 51, maxColorValue = 255)
blue.ptol <- rgb(0, 68, 136, maxColorValue = 255)
islandData %>%
    ggplot(aes(Group, Difference, fill = Group)) +
    geom_boxplot() +
    theme_tufte() +
    geom_hline(yintercept = c(0, 0.5, 1), alpha = 0.05) +
    guides(fill = FALSE) +
    theme(axis.title.y = element_text(size = 14),
          axis.title.x = element_blank(),
          axis.text = element_text(size = 14),
        plot.caption = element_text(size = 12, hjust = 0.5)) +
    labs(#caption = 'Figure 1',
         y = paste0('Cortisol Level Differences (', expression(mu), 'g/dL)')) +
    scale_fill_manual(values = c(yellow.ptol, blue.ptol)) + 
    labs(caption = 'Figure 1')
```

Figures 2 and  3
```{r, echo=TRUE, results = 'hide', fig.show = 'hide'}
ctrlDensPlot <-
    islandData %>%
    filter(Group == 'Control') %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() +
    xlim(-0.5, 0.5) +
    scale_fill_manual(values = yellow.ptol) +
    theme_tufte() +
    labs(y = 'Frequency',
         caption ='Figure 2') +
    guides(fill = FALSE) +
    theme(plot.caption = element_text(hjust = 0.5))

trtDensPlot <-
    islandData %>%
    filter(Group == 'Treatment') %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() +
    xlim(0.25, 1.5) +
    scale_fill_manual(values = blue.ptol) +
    theme_tufte() +
    labs(y = 'Frequency',
         caption = 'Figure 3') +
    guides(fill = FALSE) +
    theme(plot.caption = element_text(hjust = 0.5))

toGetLengend <-
    islandData %>%
    ggplot(aes(Difference, fill = Group)) +
    geom_density() + 
    scale_fill_manual(values = c(yellow.ptol, blue.ptol))
legend <- get_legend(toGetLengend)

plots <- align_plots(ctrlDensPlot, legend, trtDensPlot, align = 'v', axis = 'l')
plot_grid(plots[[1]], plots[[2]], plots[[3]], nrow = 1)
```

Check for any cofounders in the data set
```{r, fig.show = 'hide'}
## see if village, house number, or age makes a difference
islandData <-
    islandData %>%
    mutate(
        HouseRange = cut(as.numeric(House.Number),
                         breaks = c(0,
                                    20,
                                    400,
                                    600,
                                    800,
                                    1000,
                                    1200)),
        AgeRange = cut(as.numeric(Age),
                       breaks = c(17,
                                  30,
                                  40,
                                  50,
                                  60,
                                  70,
                                  80))
    ) %>%
    rename('HouseNumber' = House.Number)

islandData$Village <- as.factor(islandData$Village)

###########################################################
###########################################################
## effects of age
###########################################################
###########################################################

age.mod <- lm(Difference ~ Age, islandData)
par(mfrow = c(2, 2))
plot(age.mod)
## residuals are not too bad
## normality is not great, but it seems equally skewed to both sides

islandData %>%
    group_by(Age) %>%
    tally()
## unbalanced so cannot use type I anova

Anova(age.mod, type = 2)
## so we see here that the age does not appear to affect the difference

## what about when we consider age ranges
age.mod2 <- lm(Difference ~ AgeRange, islandData)
plot(age.mod2)

## couldn't find any transformations that made the residual variance or normality any better

islandData %>%
    group_by(AgeRange) %>%
    tally()
## not balanced

Anova(age.mod2, type = 2)
## lower p-value than before but still not significant

###########################################################
###########################################################
## now move on and explore house number
###########################################################
###########################################################

house.mod <- lm(Difference ~ HouseNumber, islandData)
par(mfrow = c(2, 2))
plot(house.mod)
## residuals are not too bad
## normality is not good, but it seems equally skewed to both sides

islandData %>%
    group_by(HouseNumber) %>%
    tally() %>%
    distinct(n)
## unbalanced so cannot use type I anova

Anova(house.mod, type = 2)
## so we see here that the HouseNumber does not appear to affect the difference

## what about when we consider house ranges
house.mod2 <- lm(Difference ~ HouseRange, islandData)
plot(house.mod2)

## couldn't find any transformations that made the residual variance or normality any better

islandData %>%
    group_by(HouseRange) %>%
    tally()
## not balanced

Anova(house.mod2, type = 2)
## lower p-value than before but still not significant

###########################################################
###########################################################
## now move on and explore village
###########################################################
###########################################################

village.mod <- lm(Difference ~ Village, islandData)
par(mfrow = c(2, 2))
plot(village.mod)
## residuals are not too bad
## normality is not good, but it seems equally skewed to both sides

islandData %>%
    group_by(Village) %>%
    tally() %>%
    distinct(n)
## unbalanced so cannot use type I anova

Anova(village.mod, type = 2)
## so we see here that the Village does not appear to affect the difference

## what about when we consider house ranges
house.mod2 <- lm(Difference ~ HouseRange, islandData)
plot(house.mod2)

## couldn't find any transformations that made the residual variance or normality any better

islandData %>%
    group_by(HouseRange) %>%
    tally()
## not balanced

Anova(house.mod2, type = 2)
## lower p-value than before but still not significant

#################################################################
#################################################################
### include all factors
#################################################################
#################################################################

diff.mod <- lm(Difference ~ Age * Village * HouseNumber, islandData)
plot(diff.mod)

counts <- islandData %>%
    group_by(Age, Village) %>%
    tally()
unique(counts$n)

Anova(diff.mod, type = 2)

## treat village as a blocking factor
diff.mod2 <- lm(Difference ~ Village + AgeRange + HouseRange, islandData)
plot(diff.mod2)

Anova(diff.mod2, type = 2)
summary(diff.mod2)
## appears that AgeRange is marginally significant, specifically, age range 31 to 40

islandData %>%
    group_by(AgeRange) %>%
    tally()
## but looking at the size of each age range shows that the age range 31 to 40 had ## significantly more subjects than on
```

\pagebreak

## Bibliography
